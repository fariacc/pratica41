package utfpr.ct.dainf.if62c.pratica;

public class Elipse {
	public double eixo_r;
	public double eixo_s;
  
  	public Elipse(double semi_eixo_r, double semi_eixo_s) {
            eixo_r = 2 * semi_eixo_r;
            eixo_s = 2 * semi_eixo_s;
  	}
  
    public double getArea () {
      	return Math.PI * eixo_r/2 * eixo_s/2;
    }
  
    public double getPerimetro () {
      	return Math.PI * ( 3 * ( eixo_r/2 + eixo_s/2 ) - Math.sqrt( ( 3*eixo_r/2 + eixo_s/2 ) * ( eixo_r/2 + 3*eixo_s/2 ) ) );
    }
}